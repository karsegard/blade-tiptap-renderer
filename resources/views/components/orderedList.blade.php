@props(['block'])
@php
    $content = $block['content'] ?? null;
@endphp
@if ($content)
    <ol class="my-[1.2em] space-y-4">
        @foreach ($content as $idx => $block)
            <li
                style="--ordered-list-id:'{{ $idx + 1 }}'"
                class="flex flex-row gap-4 before:w-[calc(1.2em_-_16px)] before:flex-none before:font-secondary before:typo-title before:content-[var(--ordered-list-id)]">
                @foreach ($block['content'] as $b)
                    @php
                        $component = 'tiptap::' . $b['type'];
                    @endphp

                    <x-dynamic-component :component="$component" :block="$b" />
                @endforeach
            </li>
        @endforeach
    </ol>
@endif
