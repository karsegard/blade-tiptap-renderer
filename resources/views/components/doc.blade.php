@props(['block'])

@foreach($block['content'] as $block)
    <x-tiptap::render :blocks="$block"/>
@endforeach