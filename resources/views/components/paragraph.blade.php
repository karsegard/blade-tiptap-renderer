@props(['block'])
@php
    $content = $block['content'] ?? null;
@endphp
@if($content)
<p>
    @foreach ($content as $block)
        @php
            $component = 'tiptap::paragraph-'.$block['type'];
        @endphp
        
        <x-dynamic-component :component="$component" :block="$block"/>
    @endforeach
</p>
@endif