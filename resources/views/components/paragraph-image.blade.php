@props(['block'])
@php
    $attrs = new \Illuminate\View\ComponentAttributeBag($block['attrs']??[]);
@endphp

<img {{ $attrs }}/>