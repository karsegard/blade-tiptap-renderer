@props(['block'])

@php
    $text = $block['text'];
    $marks = $block['marks'] ?? [];
    $content = collect($marks)->reduce(
        function ($carry, $marking) {
            if(!isset($carry['attrs']['class'])){
                $carry['attrs']['class'] = '';
            }
            $carry['attrs']['class'] .= match ($marking['type']) {
                'italic' => 'italic',
                'bold' => 'font-bold',
                default => '',
            };
            
            $carry['tag'] = $marking['type'] == 'link' ? 'a' :$carry['tag'];
            $carry['attrs'] = array_merge(
                $carry['attrs'], 
                $marking['attrs'] ?? []
            );
            return $carry;
        },
        ['tag' => 'span', 'attrs' => []],
    );
   
    $tagAttributes = new \Illuminate\View\ComponentAttributeBag($content['attrs']);
    
@endphp

@if (!blank($content['tag']))
    <{!! $content['tag'] !!} {{ $tagAttributes }}>
@endif
{{ $text }}
@if (!blank($content['tag']))
    </{!! $content['tag'] !!}>
@endif
