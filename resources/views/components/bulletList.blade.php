@props(['block'])
@php
    $content = $block['content'] ?? null;
@endphp
@if ($content)
    <ul class="my-[1.2em] space-y-4">
        @foreach ($content as $idx => $block)
           <li class="flex flex-row gap-4 before:w-[calc(1.2em_-_16px)] before:flex-none before:typo-title before:content-['•']">
                @foreach ($block['content'] as $b)
                    @php
                        $component = 'tiptap::' . $b['type'];
                    @endphp

                    <x-dynamic-component :component="$component" :block="$b" />
                @endforeach
            </li>
        @endforeach
    </ul>
@endif
