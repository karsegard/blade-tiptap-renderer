@props([
    'blocks'=>[]
])

@php
    $blocks = isset($blocks['type']) ? [$blocks] : $blocks;
    
@endphp


    {{$slot}}

@if(is_array($blocks))
    @foreach($blocks as $block)


        @php
            $component = 'tiptap::'.$block['type'];
        @endphp

        <x-dynamic-component :component="$component" :block="$block"/>

    @endforeach
@else
    {!! $blocks !!}
@endif
